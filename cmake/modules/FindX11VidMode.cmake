# - Try to find the X11 VidMode extension
# Once done this will define
#
#  X11VIDMODE_FOUND - system has the X11 VidMode extension
#  X11VIDMODE_INCLUDE_DIR - the corresponding include directory
#  X11VIDMODE_LIBRARIES - the corresponding libraries

# Copyright (c) 2006, Brad Hards, <bradh@frogmouth.net>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.


if (X11VIDMODE_INCLUDE_DIR AND X11VIDMODE_LIBRARIES)
  # Already in cache, be silent
  set(X11VidMode_FIND_QUIETLY TRUE)
endif (X11VIDMODE_INCLUDE_DIR AND X11VIDMODE_LIBRARIES)

FIND_PATH(X11VIDMODE_INCLUDE_DIR X11/extensions/xf86vmode.h)

FIND_LIBRARY(X11VIDMODE_LIBRARIES NAMES Xxf86vm)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(X11VidMode DEFAULT_MSG X11VIDMODE_INCLUDE_DIR X11VIDMODE_LIBRARIES )

# show the variables only in the advanced view
MARK_AS_ADVANCED(X11VIDMODE_INCLUDE_DIR X11VIDMODE_LIBRARIES )


