macro_optional_add_subdirectory(kget)
macro_optional_add_subdirectory(kopete)
macro_optional_add_subdirectory(krdc)

if(Q_WS_X11)
  macro_optional_add_subdirectory(kppp)

  if(LIBVNCSERVER_FOUND)
    macro_optional_add_subdirectory(krfb)
  endif(LIBVNCSERVER_FOUND)
endif(Q_WS_X11)
