/*
    msn p2p protocol

    Copyright (c) 2003-2005 by Olivier Goffart        <ogoffart@kde.org>

    Kopete    (c) 2002-2007 by the Kopete developers  <kopete-devel@kde.org>

    *************************************************************************
    *                                                                       *
    * This program is free software; you can redistribute it and/or modify  *
    * it under the terms of the GNU General Public License as published by  *
    * the Free Software Foundation; either version 2 of the License, or     *
    * (at your option) any later version.                                   *
    *                                                                       *
    *************************************************************************
*/


#include "msnp2poutgoing.h"
#include "msnp2pdisplatcher.h"

// qt
#include <qregexp.h>
#include <qfile.h>
#include <qtextcodec.h>
#include <qtimer.h>
//Added by qt3to4:
#include <QByteArray>

// kde
#include <kdebug.h>
#include <klocale.h>
#include <kglobal.h>



MSNP2POutgoing::MSNP2POutgoing( unsigned long int sessionID , MSNP2PDisplatcher *parent )
	: MSNP2P(sessionID , parent)
{
	m_Sfile=0L;
}

MSNP2POutgoing::~MSNP2POutgoing()
{
	delete m_Sfile;
}


void MSNP2POutgoing::parseMessage(MessageStruct &msgStr)
{
	MSNP2P::parseMessage(msgStr);

	QString dataMessage=QByteArray((msgStr.message.data()+48) , msgStr.dataMessageSize);
	kDebug(14141) <<" dataMessage: "  << dataMessage;

	if (dataMessage.contains("BYE"))
	{
		m_parent->finished(this);
	}
#if MSN_NEWFILETRANSFER
	else if (dataMessage.contains("OK"))
	{//this is a file transfer.
		if(dataMessage.contains("application/x-msnmsgr-sessionreqbody"))
		{ //first OK
			//send second invite
			QString content="Bridges: TCPv1\r\n"
					"NetID: 0\r\n"
					"Conn-Type: Restricted-NAT\r\n"
					"UPnPNat: false\r\n"
					"ICF: false\r\n\r\n"; 
			makeMSNSLPMessage( INVITE , content );
		}
		else //if(dataMessage.contains("application/x-msnmsgr-transreqbody"))
		{
			m_Sfile->open(QIODevice::ReadOnly);
			m_footer='\2';
			m_totalDataSize=m_Sfile->size();
			m_offset=0;
			kDebug(14140) <<" ok "  << m_Sfile->size();
			slotSendData();
			//start to send
			
		}
	}
#endif

}


void MSNP2POutgoing::slotSendData()
{
	kDebug(14140) ;
	char ptr[1200];
	char *data;
	int bytesRead =0;
	if(m_Sfile)
	{
		bytesRead=m_Sfile->read( ptr,1200 );
		data=ptr;
	}
	else if(m_imageToSend.size()>0)
	{
		data=m_imageToSend.data()+m_offset;
		bytesRead=qMin(1200, m_imageToSend.size()-m_offset);
	}
	else return;

	QByteArray dataBA(bytesRead);
	for (  int f = 0; f < bytesRead; f++ )
		dataBA[f] = data[f];

//	kDebug(14140) << "MSNP2PDisplatcher::slotSendData: offset="  << m_offset << "  size=" << bytesRead << "   totalSize=" << m_totalDataSize << "     sent=" << m_offset+bytesRead;

	sendP2PMessage(dataBA);

	if( m_totalDataSize == 0  ) //this has been reseted bacause the file is completely send
	{
//		kd(14140) << "MSNP2PDisplatcher::slotSendData: FINISHED! wait for the BYE message" <<   endl;
		delete m_Sfile;
		m_Sfile=0L;
		m_imageToSend=QByteArray();
		m_footer='\0';
	}
	else
		QTimer::singleShot( 10, this, SLOT(slotSendData()) );
}


#include "msnp2poutgoing.moc"
